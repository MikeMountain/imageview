import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BlenderGalleryComponent} from "./components/blender-gallery/blender-gallery.component";
import {ScreenshotsGalleryComponent} from "./components/screenshots-gallery/screenshots-gallery.component";
import {FeaturedGalleryComponent} from "./components/featured-gallery/featured-gallery.component";

const routes: Routes = [
  {path: 'featured', component: FeaturedGalleryComponent},
  {path: 'blender', component: BlenderGalleryComponent},
  {path: 'screenshots', component: ScreenshotsGalleryComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryRoutingModule {
}
