import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {GalleryRoutingModule} from './gallery-routing.module';
import {BlenderGalleryComponent} from './components/blender-gallery/blender-gallery.component';
import {ScreenshotsGalleryComponent} from './components/screenshots-gallery/screenshots-gallery.component';
import {FeaturedGalleryComponent} from "./components/featured-gallery/featured-gallery.component";

@NgModule({
  declarations: [
    BlenderGalleryComponent,
    ScreenshotsGalleryComponent,
    FeaturedGalleryComponent
  ],
  imports: [
    CommonModule,
    GalleryRoutingModule
  ]
})
export class GalleryModule {
}
