import {Injectable} from '@angular/core';
import {Store, StoreConfig} from '@datorama/akita';
import {createGallery, Gallery, GalleryItem} from "./gallery.model";

export interface GalleryState {
  images: GalleryItem[],
  featuredGallery: Gallery,
  blenderGallery: Gallery,
  screenshotGallery: Gallery
}

export function createInitialState(): GalleryState {
  return {
    images: [],
    featuredGallery: createGallery({}),
    blenderGallery: createGallery({}),
    screenshotGallery: createGallery({})
  };
}

@Injectable({providedIn: 'root'})
@StoreConfig({name: 'gallery'})
export class GalleryStore extends Store<GalleryState> {

  constructor() {
    super(createInitialState());
  }

}

