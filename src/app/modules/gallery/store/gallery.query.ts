import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { GalleryStore, GalleryState } from './gallery.store';

@Injectable({ providedIn: 'root' })
export class GalleryQuery extends Query<GalleryState> {

  constructor(protected store: GalleryStore) {
    super(store);
  }

}
