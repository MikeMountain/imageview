import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GalleryStore} from './gallery.store';
import {createGallery, Gallery, GalleryItem} from './gallery.model';
import {tap} from 'rxjs/operators';
import {NgmHttpService} from "ngm-http";
import {Observable} from "rxjs";

@Injectable({providedIn: 'root'})
export class GalleryService extends NgmHttpService<GalleryItem[]> {

  constructor(protected http: HttpClient,
              private galleryStore: GalleryStore) {
    super(http)
  }

  public getAllImages(): Observable<GalleryItem[]> {
    const url = 'http://localhost:1337/gallery-images';
    return super._get(url).pipe(
      tap((images) => {
        this.galleryStore.update({
          images: images,
          featuredGallery: this.setGallery('Featured', images, (item: GalleryItem) => item.isFeatured),
          blenderGallery: this.setGallery('Blender', images, (item: GalleryItem) => item.software === 'Blender'),
          screenshotGallery: this.setGallery('Screenshots', images, (item: GalleryItem) => item.software === 'playstation')
        })
      })
    )
  }

  private setGallery(title: string, images: GalleryItem[], filterFn: any): Gallery {
    return createGallery({
      title,
      slug: title.toLowerCase(),
      images: images.filter(filterFn)
    })
  }
}
