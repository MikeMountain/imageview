export interface Gallery {
  id: string;
  title: string;
  slug: string;
  images: GalleryItem[]
}

export interface GalleryItem {
  id: string;
  title: string;
  software?: string;
  imageUrl?: string;
  isFeatured?: boolean;
}

export function createGallery(params: Partial<Gallery>) {
  return {
    id: params?.id,
    title: params?.title,
    slug: params?.slug,
    images: params.images? params.images.map(image => createGalleryItem(image)) : []
  } as Gallery;
}

export function createGalleryItem(params: Partial<GalleryItem>) {
  return {
    id: params?.id,
    title: params?.title,
    software: params?.software,
    imageUrl: params?.imageUrl,
    isFeatured: params?.isFeatured
  } as GalleryItem;
}
