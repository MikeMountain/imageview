import {Component, OnInit} from '@angular/core';
import {CoreService} from "../../../core/services/core.service";
import {GalleryService} from "../../store/gallery.service";
import {GalleryQuery} from "../../store/gallery.query";
import {GalleryItem} from "../../store/gallery.model";

@Component({
  selector: 'app-featured-gallery',
  templateUrl: './featured-gallery.component.html',
  styleUrls: ['./featured-gallery.component.scss']
})
export class FeaturedGalleryComponent implements OnInit {

  public currentImageIndex = 0;

  constructor(private coreService: CoreService,
              private galleryService: GalleryService,
              public galleryQuery: GalleryQuery) {
  }

  ngOnInit(): void {
    if (this.galleryQuery.getValue().featuredGallery?.images?.length <= 0) {
      this.galleryService.getAllImages().subscribe();
    }
  }

  public setFeaturedImage(imageUrl: string, index: number): void {
    this.currentImageIndex = index;
    this.coreService.setActiveImage(imageUrl);
  }

  public navigateFeaturedImage(navigationType: 'next' | 'previous', images: GalleryItem[]): void {
    if (navigationType === 'next') {
      this.currentImageIndex >= images.length - 1 ?
        this.currentImageIndex = 0 :
        this.currentImageIndex++
    } else {
      this.currentImageIndex <= 0 ?
        this.currentImageIndex = images.length - 1 :
        this.currentImageIndex--
    }
    this.coreService.setActiveImage(images[this.currentImageIndex].imageUrl as string);
  }
}
