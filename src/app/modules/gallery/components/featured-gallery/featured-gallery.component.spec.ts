import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeaturedGalleryComponent} from './featured-gallery.component';

describe('FeaturedCarouselComponent', () => {
  let component: FeaturedGalleryComponent;
  let fixture: ComponentFixture<FeaturedGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FeaturedGalleryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
