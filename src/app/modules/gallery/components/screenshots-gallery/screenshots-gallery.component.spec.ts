import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenshotsGalleryComponent } from './screenshots-gallery.component';

describe('ScreenshotsGalleryComponent', () => {
  let component: ScreenshotsGalleryComponent;
  let fixture: ComponentFixture<ScreenshotsGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenshotsGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenshotsGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
