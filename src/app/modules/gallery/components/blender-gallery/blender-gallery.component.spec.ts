import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlenderGalleryComponent } from './blender-gallery.component';

describe('BlenderGalleryComponent', () => {
  let component: BlenderGalleryComponent;
  let fixture: ComponentFixture<BlenderGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlenderGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlenderGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
