import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  activeFeaturedImageSrc = new BehaviorSubject<string>('../../../../../assets/images/NatureSpirit.png');

  constructor() {
  }

  public getActiveImage(): Observable<string> {
    return this.activeFeaturedImageSrc.asObservable();
  }

  public setActiveImage(imageUrl: string): void {
    this.activeFeaturedImageSrc.next(imageUrl);
  }
}
