import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {CoreService} from "../../services/core.service";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {

  @ViewChild('container') container: ElementRef<HTMLElement> | undefined;

  constructor(private renderer: Renderer2,
              private coreService: CoreService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.setBackgroundImage()
  }

  setBackgroundImage() {
    this.coreService.getActiveImage().subscribe((imageUrl) => {
      if (this.container) {
        this.renderer.setStyle(this.container.nativeElement, 'background', `url(${imageUrl}) center no-repeat`);
        this.renderer.setStyle(this.container.nativeElement, 'background-size', '105%');
      }
    });
  }

}
